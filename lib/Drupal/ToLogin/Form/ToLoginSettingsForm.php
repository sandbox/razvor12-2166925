<?php
 
/**
 * @file
 * Contains \Drupal\Tologin\Form\FologinSettingsForm
 */
 
namespace Drupal\ToLogin\Form;
 
use Drupal\Core\Form\ConfigFormBase;
 
class ToLoginSettingsForm extends ConfigFormBase{
  
  public function getFormId()
  {
    return 'ToLogin_configure';
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state)
  {
    $config = $this->configFactory->get('ToLogin.settings');

    $form['ToLogin'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
    );
    
    $form['ToLogin']['twitter_client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter client ID'),
      '#default_value' => $config->get('twitter_client_id'),
    );
    
    
    $form['ToLogin']['twitter_client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter client secret'),
      '#default_value' => $config->get('twitter_client_secret'),
    );

    $form['ToLogin']['twitter_client_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter client token'),
      '#default_value' => $config->get('twitter_client_token'),
    );

    $form['ToLogin']['twitter_client_tokensecret'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter client token secret'),
      '#default_value' => $config->get('twitter_client_tokensecret'),
    );
    
    
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state)
  {
    $this
    ->configFactory
    ->get('ToLogin.settings')
    ->set('twitter_client_id', $form_state['values']['twitter_client_id'])
    ->set('twitter_client_secret', $form_state['values']['twitter_client_secret'])
    ->set('twitter_client_tokensecret', $form_state['values']['twitter_client_tokensecret'])
    ->set('twitter_client_token', $form_state['values']['twitter_client_token'])
    ->save();
    
    parent::submitForm($form, $form_state); 
  }
  
}