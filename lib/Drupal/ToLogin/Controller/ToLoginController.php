<?php

  
 
namespace Drupal\ToLogin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\user\RegisterFormController;
use Drupal\user;

use TwitterOAuth;

require('modules/ToLogin/src/twitteroauth/twitteroauth.php');
 
class ToLoginController extends ControllerBase{
 
  /*
  * Function for save new users from Facebook
  */  

  public function saveUser($details) {
    
    //fields for database
    $fields = array(
         'name' =>    $details->name,
         'status' =>  1,
         'created' => time(),
         'pass' =>    $details->id
       );

    
    //create new user
    $account = \Drupal::entityManager()
            ->getStorageController("user")
            ->create($fields);

    //save created user
    $account->save();

    //update facebook id for user. Write in pass field
    $update_fbid = db_update('users')->fields(array('pass' => $details->id))->condition('uid', $account->id(), '=')->execute();
    

    return $account;
  }

  /*
  * Function check exists user
  */
  public function existsUser($fbid){

    //sql request for users
    $sql = "SELECT pass FROM users WHERE pass = :tid";

    //query to db and set virble $fbid 
    $result = db_query($sql, array(
      ':tid' => $fbid,
    ));

    $strings = $result->fetchAll();
    
    //return count of users 0 or 1 (true or false)
    return (bool) count($strings);
  }

  /*
  * Get user by fbid
  */
  public function selectUserByTid($field, $fbid){
    
    //sql request for db
    $sql = "SELECT * FROM users WHERE pass = :tid LIMIT 1";
    $result = db_query($sql, array(':tid' => $fbid))->fetchAssoc();
    
    //get rows from request
    return $result[$field];
  }

  /*
  * Create Page 
  */
  public function ToLoginPage() {
    
    global $user;
    global $base_url;

    //parsing configuration file
    $config=$this->config('ToLogin.settings');

    //array with redirect parmas
    $par=array(
      'url' =>       $base_url,
      'app_id' =>    $config->get('twitter_client_id'),
      'app_secret' => $config->get('twitter_client_secret'),
      'app_access_token' => $config->get('twitter_client_token'),
      'app_access_tokensecret' => $config->get('twitter_client_tokensecret')
    );    

    //If facebook return code
    if(isset( $_GET['code'])) {

    }else{
      if(isset($_GET['redirect'])){

        //Setting up TwitterOAuth libray
        $twitter = new TwitterOAuth($par['app_id'], $par['app_secret'], $par['app_access_token'], $par['app_access_tokensecret']);
        
        //Request to Twitter
        $tw_query_result = $twitter->get('https://api.twitter.com/1.1/account/verify_credentials.json');

        

        if(isset($tw_query_result->errors)){
          
          //Return error message if error has detected 
          $build=array('#markup'=>t('Error to login with Twitter. Try again later'));
        
        }else{
          //check for logined user alredy exists
          if($this->existsUser($tw_query_result->id)){

            //if exists load user by id
            $svu=user_load($this->selectUserByTid('uid', $tw_query_result->id));

          }else{

            //if not exists create new user
            $svu=$this->saveUser($tw_query_result);
            
          }

          //Login new user 
          user_login_finalize($svu);

          //Show message and redirect to homepage
          $build=array('#markup' =>'Welcome to '.$base_url.','.$fb_query_result->{"name"}.'<meta http-equiv="refresh" content="0; url='.$base_url.'" />');

        }

      }else{
        $build=array('#markup' => t(''));
      }
      
    }

    return $build;
  }
 
} 