<?php


/**
 * @file
 * Contains \Drupal\FoLogin\Plugin\Block\FoLoginBlock.
 */



namespace Drupal\ToLogin\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config;

/**
 * Provides a '' block.
 *
 * @Block(
 *   id = "ToLoginBlock",
 *   subject = @Translation("Twitter authentication"),
 *   admin_label = @Translation("Twitter authentication")
 * )
 */
class ToLoginBlock extends BlockBase {

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    global $base_url;

    //Generate url to login
    return array(
        '#type' => 'markup',
        '#markup' => '<a href="'.$base_url.'/tlogin?redirect=1">Login with Twitter</a>'
      );
  }

}
